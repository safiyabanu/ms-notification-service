
var http = require("http");
 
var app     = require('express')();

var server  = http.createServer(app);

var WebSocketServer = require('ws').Server;

var wss = new WebSocketServer({ server: server });

var redis = require("redis");

var config = require("./config");

var mysql = require('mysql');
   
var SNSClient = require('aws-snsclient');

var redisdb = config.redis;

var publisher = redis.createClient({host :redisdb.host, port : redisdb.port});

var redisSub = redis.createClient({host :redisdb.host, port : redisdb.port});

process.setMaxListeners(0);

publisher.on('ready',function() {
 console.log("Redis is ready");
});

publisher.on('error',function(e) {
 console.log(e);
});

var db = config.database;
var db_connection = mysql.createConnection({ host : db.host,
  user     : db.user,
  password : db.password,
  database : db.database
});

require('events').EventEmitter.defaultMaxListeners = Infinity;

app.get('/v1/socket-test', function(req, res){
   res.sendFile(__dirname + '/wsclient.html');
});

var clients = {};

//app.get('/notifications', function(req, res){

  wss.on('connection', function connection(ws) {
    try { 
      ws.send('Welcome socket server'); 
    }
   catch (e) {
      console.log(e); 
    }
    var userid = ws.upgradeReq.url.split('v1/notifications/')[1];
    var id = ws.upgradeReq.headers['sec-websocket-key'];
    clients[id] = {};
    var final_set_followers=[],final_set_showrooms=[],mute_listers_array = [],final_mute_listers_array = [];
      console.log(userid);
      if(userid){
        db_connection.query('SELECT CONCAT("user_",FL_suserid) AS FL_suserid FROM yaac_fol_addfollowing WHERE FL_cuserid = '+userid+' AND status=1',function(err, followers, fields) {

          db_connection.query('SELECT muter_user_id  FROM custom_showroom_mute_users WHERE user_id = '+userid+'',function(err,mutelist, fields) {
            //console.log(rows);
          db_connection.query('SELECT CONCAT("showroom_",showroom_id) AS showroom_id  FROM custom_showroom_favorite WHERE user_id = '+userid+' AND follow_status="follow"',function(err, showrooms, fields) {
            if(followers){
              var final_set_followers = followers.map(function (follow) {
               return follow.FL_suserid;
              });
            }
            if(showrooms){
              var final_set_showrooms = showrooms.map(function (showroom) {
                return showroom.showroom_id;
              });
            }
             if(mutelist){
              var mute_listers_array = mutelist.map(function (mutted) {
                return mutted.muter_user_id;
              });
              if(mute_listers_array.length > 0 && final_set_followers.length > 0){
                var final_mute_listers_array = mute_listers_array[0].split(',');
                for (var i = 0; i < final_mute_listers_array.length; i++) {
                  var index = final_set_followers.indexOf('user_'+final_mute_listers_array[i]);
                  if (index > -1) {
                    final_set_followers.splice(index, 1);
                  }
                }
              }
            }
            if(final_set_followers.length > 0 ||  final_set_showrooms.length > 0){
              if(final_set_followers.length > 0 &&  final_set_showrooms.length > 0)
                var channel = final_set_followers.concat(final_set_showrooms);
              else if(final_set_showrooms.length > 0)
                var channel = final_set_showrooms; 
              else
                var channel = final_set_followers;
                //process.exit(1);
                  var channel_array = channel;
                  
                  redisSub.subscribe(channel);
                  redisSub.on('message', function(channel, message) {
                      if(channel !== 'user_'+userid && channel_array.indexOf(channel) > -1){
                        try { 
                          ws.send('New mint notfication received'); 
                        }
                        catch (e) {
                          console.log(e); 
                        }
                      }
                  }); 
            }
            ws.on('close', function () {
              if(channel.length > 0){
                redisSub.unsubscribe(channel);
              } 
               delete clients[ws.upgradeReq.headers['sec-websocket-key']];
            });
          });
          });   
        });
        ws.on('error', function () {
            delete clients[ws.upgradeReq.headers['sec-websocket-key']];
        });
      }
    });
//});

var auth = {
    region: process.env['REGION'] ,
    account: process.env['ACCOUNT'],
    topic: process.env['AWS_SNS_TOPIC']
}

var set_exist_array = [];
var sns_receive = SNSClient(auth, function(err, message) {
    console.log(message);
    if(message.Message){
      var json = JSON.parse(message.Message);
      var exist_channel = json.mintid;
      var channel = json.channel;
      if(exist_channel && channel){
        if(set_exist_array.indexOf(exist_channel) === -1){
          set_exist_array.push(exist_channel);
          console.log(channel);
          publisher.publish(channel, "new mint posted");
          setTimeout(function () {
              set_exist_array = [];
          },1500);
        }
      }
    }
});


app.post('/v1/sns-receiver', sns_receive);

server.listen(config.server_port, function () { console.log('Listening on') });


