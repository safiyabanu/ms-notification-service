var should = require("should")
var redis = require('redis');

var mysql = require('mysql');

var SNSClient = require('aws-snsclient');

var server = require('http').createServer()
  , url = require('url')
  , WebSocketServer = require('ws').Server
  , wss = new WebSocketServer({ server: server })

var config = require("../config");

var redisdb = config.redis;

var publisher = redis.createClient({host :redisdb.host, port : redisdb.port});

var subscriber = redis.createClient({host :redisdb.host, port : redisdb.port});

var db = config.database;

var db_connection = mysql.createConnection({ host : db.host,
  user     : db.user,
  password : db.password,
  database : db.database
});

describe("publish and subscribe", function () {
  it("check redis connection", function (done) {
    should.exist(publisher.publish);
    done();
  });
  it("should publish to a channel", function (done) {
    var channelName = "test";
    try {
      (function () {
        publisher.publish(channelName, "highgfg");
      }).should.throwError();
    } catch (e) {
      done();
    }
  });
  it("Check users followers and joined showroom list", function (done) {
    var userid = '765';
    var final_set_followers=[],final_set_showrooms=[],mute_listers_array = [],final_mute_listers_array = [];
    if(userid){
      db_connection.query('SELECT CONCAT("user_",FL_suserid) AS FL_suserid FROM yaac_fol_addfollowing WHERE FL_cuserid = '+userid+' AND status=1',function(err, followers, fields) {

        db_connection.query('SELECT muter_user_id  FROM custom_showroom_mute_users WHERE user_id = '+userid+'',function(err,mutelist, fields) {
          //console.log(rows);
        db_connection.query('SELECT CONCAT("showroom_",showroom_id) AS showroom_id  FROM custom_showroom_favorite WHERE user_id = '+userid+' AND follow_status="follow"',function(err, showrooms, fields) {
          if(followers){
            var final_set_followers = followers.map(function (follow) {
             return follow.FL_suserid;
            });
          }
          if(showrooms){
            var final_set_showrooms = showrooms.map(function (showroom) {
              return showroom.showroom_id;
            });
          }
             if(mutelist){
              var mute_listers_array = mutelist.map(function (mutted) {
                return mutted.muter_user_id;
              });
              if(mute_listers_array.length > 0 && final_set_followers.length > 0){
                var final_mute_listers_array = mute_listers_array[0].split(',');
                for (var i = 0; i < final_mute_listers_array.length; i++) {
                  var index = final_set_followers.indexOf('user_'+final_mute_listers_array[i]);
                  if (index > -1) {
                    final_set_followers.splice(index, 1);
                  }
                }
              }
            }
            if(final_set_followers.length > 0 ||  final_set_showrooms.length > 0){
              if(final_set_followers.length > 0 &&  final_set_showrooms.length > 0)
                var channel = final_set_followers.concat(final_set_showrooms);
              else if(final_set_showrooms.length > 0)
                var channel = final_set_showrooms; 
              else
                var channel = final_set_followers;
                done();
              }
              else
              {
                done();
              }
            });
          });
        });  
      }
      else
      {
        done();
      }
  });
  it("Should Publish and Subscribe channel", function (done) {
    var channelName = "user_765";
    subscriber.subscribe(channelName);
    var otherchannel = 'check';

    subscriber.on('message', function (ch, msg) {
      ch.should.equal(channelName);
      subscriber.unsubscribe(channelName);
      done();
    });
      publisher.publish(channelName, "user mint");
  });
});
  

