module.exports = {
  server_port: process.env['NODE_PORT'],
  redis:{
	host : process.env['REDIS_HOST'],
	port: process.env['REDIS_PORT']
  },
  database:{
	host : process.env['MYSQL_HOST'],
	user : process.env['MYSQL_USER'],
	password : process.env['MYSQL_PASSWORD'],
	database: process.env['MYSQL_DB']
  }
}