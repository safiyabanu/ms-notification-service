FROM node:4-onbuild
# replace this with your application's default port
# Install app dependencies
# COPY package.json /usr/src/app/
# RUN npm install

RUN apt-get update -y
RUN apt-get install vim supervisor -y
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
# replace this with your application's default port

EXPOSE 5001
CMD ["/usr/bin/supervisord"]
